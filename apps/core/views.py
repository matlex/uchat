import datetime
import json

from django.db.utils import IntegrityError
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import JsonResponse, HttpResponse
from django.views.generic import TemplateView

from .models import *
from uchat.settings import CENTRIFUGE_ADDRESS, CENTRIFUGE_SECRET, CENTRIFUGE_TIMEOUT

from cent import Client, get_timestamp, generate_token


class GeneralChatView(TemplateView):
    """
    A general chat implementation where non-registered and registered users can see messages in that chat,
    but only registered users will see message input field and post new messages to chat.
    """
    template_name = 'core/general_chat.html'

    def get_context_data(self, **kwargs):
        context = super(GeneralChatView, self).get_context_data(**kwargs)
        channel_name = "public:service_channel"  # Will use a service_channel for signalling from back to front
        timestamp = get_timestamp()
        user = str(self.request.user.id) if self.request.user.is_authenticated else str("")
        token = generate_token(CENTRIFUGE_SECRET, user, timestamp)

        # Now we need to show messages of current chat for user.
        # If user wasn't here then he doesn't have LastViewedMessage record, and then we will show last 50 msgs in chat,
        # otherwise we will show new messages + some old messages too when opening general chat page.
        general_chat_obj = Chat.objects.get_or_create(title="General chat")[0]
        if self.request.user.is_authenticated:  # When user is authorized
            try:
                last_viewed_message_obj = LastViewedMessage.objects.get(user=self.request.user, chat=general_chat_obj)
                old_messages_to_show = 5
                new_messages_and_some_old_to_show = Message.objects.filter(
                    chat=general_chat_obj, id__gt=last_viewed_message_obj.message.id - old_messages_to_show)
                context.update({
                    "CHAT_MESSAGES": new_messages_and_some_old_to_show
                })
                # Updating last viewed message
                last_viewed_message_obj.message = new_messages_and_some_old_to_show.last()
                last_viewed_message_obj.save()

            except LastViewedMessage.DoesNotExist:
                last_50_messages_to_show_newcomer = Message.objects.filter(chat=general_chat_obj).reverse()
                # Passing in last messages to context for displaying on client
                if last_50_messages_to_show_newcomer.exists():  # If we have any messages
                    context.update({
                        "CHAT_MESSAGES": list(last_50_messages_to_show_newcomer)[-50:]
                    })
                    # Next we need to save LastViewedMessage for user to tracking and message displaying in future
                    lvm = LastViewedMessage.objects.create(
                        user=self.request.user,
                        chat=general_chat_obj,
                        message=last_50_messages_to_show_newcomer.first()
                        # First element in reversed list = last element.
                    )
        else:
            messages_to_show = Message.objects.filter(chat=general_chat_obj)
            context.update({
                "CHAT_MESSAGES": list(messages_to_show)[-50:]
            })

        # Updating context dictionary
        context.update({
            "CENTRIFUGE_USER": user,
            "CENTRIFUGE_TIMESTAMP": timestamp,
            "CENTRIFUGE_TOKEN": token,
            "CENTRIFUGE_CHANNEL_NAME": channel_name,
            "CHAT_OBJ": general_chat_obj
        })
        return context

    @method_decorator(login_required)
    def post(self, *args, **kwargs):
        """
        Receives AJAX POST request from client and returns JSON response
        :param args:
        :param kwargs:
        :return:
        """
        message_to_add = self.request.POST.get('message_text', None)
        chat_id_to_add = self.request.POST.get('to_chat_id', None)

        print("message to add:{}, chat_id_to_add:{}".format(message_to_add, chat_id_to_add))

        chat_obj = Chat.objects.get(id=chat_id_to_add)

        if message_to_add and chat_id_to_add:
            # Now we need create a Message object and save it into DB.
            Message.objects.create(
                user=self.request.user,
                chat=chat_obj,
                message_text=message_to_add
            )

            # Next we have to pass in message into Centrifugal
            client = Client(CENTRIFUGE_ADDRESS, CENTRIFUGE_SECRET, timeout=CENTRIFUGE_TIMEOUT)

            # Publish data into channel
            service_channel_data = {"signal_to_client": "new_message_published"}
            client.publish("public:service_channel", service_channel_data)

            # Response payload for client just for debugging yet
            data = {
                'result': "success",
                "message": "Hey, i've got your message!"
            }
        # If something was empty, do nothing and return some info message
        else:
            data = {
                'result': "failed",
                "message": "Hey, your message or channel is None!"
            }
        return JsonResponse(data)


def get_new_messages(request, **kwargs):
    if request.is_ajax():
        current_chat = Chat.objects.get(pk=kwargs.get("chat_id"))
        if request.user.is_authenticated:
            # In case if this is a new chat there is no last_viewed_message records for this particular chat
            # so we have to handle that case with some if's and try:except constructions.
            try:
                last_seen_message_by_client = LastViewedMessage.objects.get(user=request.user, chat=current_chat)
            except LastViewedMessage.DoesNotExist:
                last_seen_message_by_client = None

            try:
                if last_seen_message_by_client:
                    new_message = \
                        Message.objects.filter(chat=current_chat, id__gt=last_seen_message_by_client.message.id)[0]
                    message_to_return = {
                        "username": "%s %s" % (new_message.user.first_name, new_message.user.last_name),
                        "date": new_message.datetime.strftime('%m/%d/%y'),
                        "time": new_message.datetime.strftime('%H:%M:%S'),
                        "message_text": new_message.message_text
                    }
                    last_seen_message_by_client.message = new_message
                    last_seen_message_by_client.save()
                else:
                    new_message = Message.objects.filter(chat=current_chat).last()
                    LastViewedMessage.objects.create(user=request.user, chat=current_chat, message=new_message)
                    message_to_return = {
                        "username": "%s %s" % (new_message.user.first_name, new_message.user.last_name),
                        "date": new_message.datetime.strftime('%m/%d/%y'),
                        "time": new_message.datetime.strftime('%H:%M:%S'),
                        "message_text": new_message.message_text
                    }
            except (IntegrityError, IndexError):
                message_to_return = {"status": "no new messages"}
        else:
            try:
                # For anonymous user we will show only one new message!
                new_message = Message.objects.filter(chat=current_chat).last()
                message_to_return = {
                    "username": "%s %s" % (new_message.user.first_name, new_message.user.last_name),
                    "date": new_message.datetime.strftime('%m/%d/%y'),
                    "time": new_message.datetime.strftime('%H:%M:%S'),
                    "message_text": new_message.message_text
                }
            except AttributeError:
                message_to_return = {"status": "no new messages"}

        return HttpResponse(json.dumps(message_to_return))
