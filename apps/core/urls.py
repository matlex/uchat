from django.conf.urls import url
from .views import GeneralChatView, get_new_messages


urlpatterns = [
    url(r'general/$', GeneralChatView.as_view(), name='general_chat'),
    url(r'(?P<chat_id>[0-9])/new_messages', get_new_messages, name='ajax_new_messages')
]
