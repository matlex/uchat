from django.db import models
from django.contrib.auth.models import User


class Chat(models.Model):
    title = models.CharField(max_length=255, unique=True)


class Message(models.Model):
    user = models.ForeignKey(User)
    chat = models.ForeignKey(Chat)
    message_text = models.CharField(max_length=255)
    datetime = models.DateTimeField(auto_now_add=True)

    def natural_key(self):
        return (self.id, self.user.first_name)

    def __unicode__(self):
        return "{}: {}".format(self.user.username, self.message_text)

    def __str__(self):
        return "{}: {}".format(self.user.username, self.message_text)


class LastViewedMessage(models.Model):
    user = models.ForeignKey(User)
    chat = models.ForeignKey(Chat)
    message = models.ForeignKey(Message)
