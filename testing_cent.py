import os
import datetime
from pprint import pprint
import django
from cent import Client


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "uchat.settings")
django.setup()

url = "http://localhost:8001"
secret_key = '5d730ff1-5686-43c2-9570-b052a6e1f3cb'

# initialize client instance.
client = Client(url, secret_key, timeout=1)

# publish data into channel
channel = "public:service_channel"
data = {
    "signal": "new_message_in_chat_was_sent"
}
client.publish(channel, data)

# other available methods
client.unsubscribe("Matt")
client.disconnect("Matt")
# messages = client.history("public:service_channel")
clients = client.presence("public:service_channel")
channels = client.channels()
stats = client.stats()

pprint(clients, indent=4)
